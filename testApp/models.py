from __future__ import unicode_literals
from django.db import models
# Create your models here.
class Meal(models.Model):		
	flavor = models.CharField(max_length=120)
	name = models.CharField(max_length=300,primary_key=True)
	rating = models.IntegerField(default=1)
	def __str__(self):
		return self.name
	def getPrice(self):
		price = 0
		for i in self.ingredient_set.all():
			price += i.price
		return price
	def getName(self):
		return ' '.join(self.name.split('-'))

class Ingredient(models.Model):
	name = models.CharField(max_length=200) 
	price = models.IntegerField(default=0)
	meal = models.ForeignKey(Meal,on_delete=models.CASCADE)
	
	def __str__(self):
		return self.name + ' costs '+ str(self.price) +' dollars'
	def pricePlus(self):
		self.price += 1 
	def priceMinus(self):
		if(self.price>=1):
			self.price -=1
		else:
			self.price = 0 
	

