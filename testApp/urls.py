from django.conf.urls import url
from . import views
app_name = 'testApp'
urlpatterns = [
	
	url(r'^/$',views.IndexView,name='default'),
	
	url(r'/(?P<pk>[\w-]*)/*',views.MealsList,name='wtvman'),
	
]
