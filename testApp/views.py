from django.shortcuts import render
from django.views import generic
from .models import Meal,Ingredient
from django.http import HttpResponse
# Create your views here.
def MealsList(request,pk):
	all = True
	if(request.POST.get('priceUp')):
		ing = Ingredient.objects.get(pk=request.POST['priceUp'])
		ing.pricePlus()
		ing.save()

	if(request.POST.get('priceDown')):
		ing = Ingredient.objects.get(pk=request.POST['priceDown'])
		ing.priceMinus()
		ing.save()
	
	if(request.POST.get('adding')):
		i1 = Ingredient()
		i1.name = request.POST['adding']
		i1.meal = Meal.objects.get(pk=pk)
		i1.save()
	if(request.POST.get('ff')):
		all = False
	if(request.POST.get('sf')):
		all = True
	
	context = {
	'meal' : Meal.objects.get(pk=pk),
	'all':all
	}
	print str(all)
	template_name = 'testApp/ingredients.html'
	return render(request,template_name,context)
def DefaultView(request):
	return HttpResponse('default view it is')
def IndexView(request):
	template_name='testApp/defaultMealsList.html'
	context = {'meals':Meal.objects.all()}
	return render(request,template_name,context)
